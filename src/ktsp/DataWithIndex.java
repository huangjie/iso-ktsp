package ktsp;

/**
 *
 * @author michal
 *
 * this small class is merely used as an auxiliary tuple for the construction of the ranking.
 * It groups the numeric data with the original index it had in the column before ordering, so after ordering the column
 * it is possible to place each order in the ranking.
 * The most important feature of this class is that it implements Comparable and the compareTo method.
 */
public class DataWithIndex implements Comparable<DataWithIndex> {
    Double data;
    int index;

    DataWithIndex(double d, int i) {
        data = d;
        index = i;
    }

    public int compareTo(DataWithIndex other) {
        return data.compareTo(other.data);

        
    }
}
