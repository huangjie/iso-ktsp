package ktsp;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.List;

/**
 *
 * @author michal
 *
 * This is an IO class for writing the output to a file. This class has methods to print all the output data that is generated
 * in the program with the corresponding format. These methods print line by line, each method corresponding to a different
 * output type with its format. Note that the output is written at many places of the program and is not delayed to the end of
 * the run, meaning that if an error occurs, the output file may or may not have been created and it may contain partial output.
 */
public class FileWriter {
    // an iteration counter to keep track of the iterations when needed. This is necessary because in some places where
    // iteration output is written, the iteration count is not available in the scope that calls the writer.
    private int iteration;
    // Java IO class that manages output to a file.
    private PrintWriter outputFile = null;
    // the strings that are used as class marks. These are the same as the suffixes used for class differentiation in the sample
    // names or ids
    private String class1;
    private String class2;
    // a list of the sample names or ids to easily translate the indexes used in the program to the naems or ids that should be
    // printed
    private List<String> sampleNames = null;
    // this java class is used to easily apply the same format to all the decimal data printed.
    DecimalFormat df;

    // the constructor of the class, it initialises all the necessary attributes of the class and creates and opens the output
    // file. It also handles some common errors when creating the file.
    FileWriter(String filename, String c1, String c2, List<String> sn) {
        sampleNames = sn;
        class1 = c1;
        class2 = c2;
        iteration = 0;
        df = new DecimalFormat();
        df.setMaximumFractionDigits(4);
        df.setGroupingUsed(false);
        try {
            outputFile = new PrintWriter(filename, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            System.err.println("It seems that the system does not support UTF-8 encoding. See UnsupportedEncodingException in Java documentation for more details.");
            System.exit(1);
        }
        catch (FileNotFoundException ex) {
            System.err.println("Impossible to open the output file. Check permissions or see FileNotFoundException in java documentation for more details.");
            System.exit(1);
        }
    }

    // this method closes the output channel to write the file. It should be called once any output is finished and before the
    // end of the run.
    void closeFile() {
        outputFile.close();
    }

    // this increments the iteration counter. It should be called whenever an iteration is completed, whether it is
    // a crossvalidation iteration in standard mode or an iteration of the suffle labels mode
    void nextIteration() {
        iteration++;
    }

    // All these remaining methods write a line with a concrete type of output using the corresponding format.
    // Note that tabs are used as separators between different parts of the output line.
    void printIterationKmaxPair(String first, String second, double score, double rscore) {
        outputFile.println("iteration=" + iteration + "\tkmax_pair\t" + first + "\t" + second + "\tscore1=" + df.format(score - 1) + "\tscore2=" + df.format(rscore));
    }

    void printIterationKPerformance(int k, int TN, int TT, int FN, int FT) {
        outputFile.println("iteration=" + iteration + "\tk_performance\tk=" + k + "\tT" + class1 + "=" + TN + "\tT" + class2 + "=" + TT + "\tF" + class1 + "=" + FN + "\tF" + class2 + "=" + FT);
    }

    void printKAveragePerformance(int k, double performance) {
        outputFile.println("final\tk_average_performance\tk=" + k + "\tperformance=" + performance);
    }

    void printSinglePairPerformance(String first, String second, int TN, int TT, int FN, int FT, double IG, double score, double rscore) {
        outputFile.println("final\tsingle_pair_performance\t" + first + "\t" + second + "\tT" + class1 + "=" + TN + "\tT" + class2 + "=" + TT + "\tF" + class1 + "=" + FN + "\tF" + class2 + "=" + FT + "\tIG=" + df.format(IG) + "\tscore1=" + df.format(score - 1) + "\tscore2=" + df.format(rscore));
    }

    void printFinalModelPair(String first, String second) {
        outputFile.println("final\tmodel_pair\t" + first + "\t" + second);
    }

    void printModelPerformance(int TN, int TT, int FN, int FT) {
        outputFile.println("final\tprediction_only" + "\tmodel_performance" + "\tT" + class1 + "=" + TN + "\tT" + class2 + "=" + TT + "\tF" + class1 + "=" + FN + "\tF" + class2 + "=" + FT);
    }

    void printPredictionDetails(int sample, int correctVotes, int incorrectVotes) {
        outputFile.println("final_prediction_only" + "\tprediction_details" + "\tsample=" + sampleNames.get(sample) + "\tcorrect_votes=" + correctVotes + "\tincorrectVotes=" + incorrectVotes);
    }

    void printRandomLabelResult(double score1, double score2) {
        outputFile.println("random_Label_iteration=" + iteration + "\tscore1=" + df.format(score1 - 1) + "\tscore2=" + df.format(score2));
    }


}
